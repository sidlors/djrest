"""
Common regex for validators.
"""


PASSWORD = r'^[a-zA-Z0-9-_!@#$&%]{8,200}$'
USERNAME = r'^[a-zA-Z0-9-_.@]{4,40}$'
