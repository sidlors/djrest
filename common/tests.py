from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from accounts.auth import auth_token
from accounts.models import User

from .utils import to_object, unique_id


class UniqueIdTestCase(TestCase):
    """
    Unique id test case
    """

    def test_unique_id_length(self):
        uid = unique_id()
        self.assertEqual(len(uid), 22)


class ToObjectTestCase(TestCase):
    """
    To object test case
    """

    def test_dict_to_object(self):
        obj = to_object({'pi': 3.14})
        self.assertTrue(hasattr(obj, 'pi'))


class LocalTimeTestCase(TestCase):
    """
    Local time test case
    """

    fixtures = ('users',)

    def setUp(self):
        self.user = User.objects.get(username='user')
        self.token = auth_token(self.user)
        self.client = Client()

    def tearDown(self):
        self.user.delete()

    def test_local_time(self):
        path = reverse('common_local_time', kwargs={
            'version': 'v1'
        })

        response = self.client.get(
            path,
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % self.token
        )

        data = response.json()

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )
        self.assertTrue('localTime' in data.keys())
