from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from rest_framework import serializers

from . import mixins
from .tasks import send_fcm_notification, send_mail
from .utils import to_object


class FruitListSerializer(mixins.QueryFieldsMixin,
                          serializers.Serializer):
    """
    Fruit list serializer
    """

    id = serializers.CharField(max_length=22)
    name = serializers.CharField(max_length=50)


class LocalTimeSerializer(serializers.Serializer):
    """
    Local time serializer
    """

    localTime = serializers.DateTimeField()


class FCMSendPushNotificationSerializer(serializers.Serializer):
    """
    FCM send push notification serializer
    """

    title = serializers.CharField(max_length=100)
    body = serializers.CharField(max_length=150)

    to = serializers.CharField(max_length=200)

    def create(self, validated_data):
        send_fcm_notification.delay(**validated_data)
        return to_object(validated_data)


class SendEmailSerializer(serializers.Serializer):
    """
    Send email serializer
    """

    subject = serializers.CharField(max_length=120)
    body = serializers.CharField(max_length=150)

    to = serializers.EmailField()

    def create(self, validated_data):
        send_mail.delay(**validated_data)
        return to_object(validated_data)


class SendRoomMessageSerializer(serializers.Serializer):
    """
    Send room message serializer
    """

    room = serializers.CharField(max_length=40)
    content = serializers.CharField(max_length=250)

    def create(self, validated_data):
        channel_layer = get_channel_layer()

        group_send = async_to_sync(channel_layer.group_send)
        group_send('room-%s' % validated_data['room'], {
            'type': 'chat.message',
            'text': validated_data['content']
        })
        return to_object(validated_data)
