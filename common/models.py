from django.db import models

from .utils import get_object_or_none, unique_id


class BaseModel(models.Model):
    """
    Base model
    """

    id = models.CharField(
        max_length=22,
        editable=False, primary_key=True)

    def __make_id(self):
        uid = unique_id()[:11]  # Limited to 11 characters
        obj = get_object_or_none(self.__class__, pk=uid)
        return uid if not obj else ''

    def save(self, *args, **kwargs):
        while not self.id:
            self.id = self.__make_id()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.pk

    class Meta:
        abstract = True
