from django.urls import re_path as path

from . import views


urlpatterns = [
    path(
        r'^fruits$',
        views.FruitAPIView.as_view(),
        name='common_fruit_list'
    ),
    path(
        r'^local-time$',
        views.LocalTimeAPIView.as_view(),
        name='common_local_time'
    ),
    path(
        r'^serve/(?P<path>.*)$',
        views.ServeAPIView.as_view(),
        name='common_serve'
    ),
    path(
        r'^fcm/send-push-notification$',
        views.FCMSendPushNotificationAPIView.as_view(),
        name='common_fcm_send_push_notification'
    ),
    path(
        r'^send-email$',
        views.SendEmailAPIView.as_view(),
        name='common_send_email'
    ),
    path(
        r'^send-room-message$',
        views.SendRoomMessageAPIView.as_view(),
        name='common_send_room_message'
    ),
]
