from base64 import urlsafe_b64encode as b64encode
from uuid import uuid4

from django.conf import settings
from django.http import Http404
from requests import request as _request


def get_object_or_none(klass, *args, **kwargs):
    """
    Get object or none
    """
    queryset = klass._default_manager.all()\
        if hasattr(klass, '_default_manager') else klass
    try:
        obj = queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        obj = None
    return obj


def get_object_or_404(klass, *args, **kwargs):
    """
    Get object or 404
    """
    obj = get_object_or_none(klass, *args, **kwargs)
    if not obj:
        raise Http404
    return obj


def unique_id():
    """
    Generate unique id
    """
    return b64encode(uuid4().bytes).decode()[:-2]


def to_object(data):
    """
    Data to object
    """
    iterable = (list, tuple, set)
    if isinstance(data, iterable):
        return [to_object(i) for i in data]

    if not isinstance(data, dict):
        return data

    obj = type('Obj', (), {})()
    for k, v in data.items():
        setattr(obj, k, to_object(v))
    return obj


def request(method, url, **kwargs):
    """
    Make http request
    """
    try:
        response = _request(method, url, **kwargs)
        response.status = response.status_code
    except Exception:
        response = to_object({})
        response.status = 500  # Internal server error
    return response


def send_notification(title='', body='', icon='', data={}, to=[]):
    """
    Send firebase push notification
    """
    if not to:
        return None

    return request(
        'POST',
        settings.FCM_URL,
        headers={
            'Content-Type': 'application/json',
            'Authorization': 'Key=%s' % settings.FCM_TOKEN
        },
        json={
            'registration_ids': to,
            'notification': {  # Max. 2KB.
                'title': title,
                'body': body,
                'icon': icon or settings.FCM_DEFAULT_ICON
            },
            'data': data  # Max. 4KB.
        }
    )
