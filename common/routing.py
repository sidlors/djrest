from django.urls import re_path as path

from . import consumers


urlpatterns = [
    path(
        r'^common/rooms/(?P<room_id>[0-9]{1,20})$',
        consumers.ChatConsumer,
        name='common-chat'
    ),
]
