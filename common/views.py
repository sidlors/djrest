from django.conf import settings
from django.utils import timezone
from django.views.static import serve
from rest_framework import status
from rest_framework.views import APIView

from accounts.permissions import AllowAny

from . import generics, serializers
from .response import Response
from .utils import to_object, unique_id


class FruitAPIView(generics.ListAPIView):
    """
    Fruit api view
    """

    queryset = []
    serializer_list_class = serializers.FruitListSerializer

    filter_backends = ()

    def get_queryset(self):
        self.queryset = [{
            'id': unique_id(),
            'name': 'Apple {}'.format(n+1)
        } for n in range(1000)]

        return self.queryset


class LocalTimeAPIView(generics.RetrieveAPIView):
    """
    Local time api view
    """

    serializer_list_class = serializers.LocalTimeSerializer

    def get_object(self):
        return to_object({'localTime': timezone.now()})


class ServeAPIView(APIView):
    """
    Serve api view
    """

    permission_classes = (AllowAny,)

    def get(self, request, path, *args, **kwargs):
        return serve(
            request,
            path,
            document_root=settings.MEDIA_ROOT
        )


class FCMSendPushNotificationAPIView(generics.CustomCreateAPIView):
    """
    FCM send push notification api view
    """

    serializer_create_class = serializers.FCMSendPushNotificationSerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK


class SendEmailAPIView(generics.CustomCreateAPIView):
    """
    Send email api view
    """

    serializer_create_class = serializers.SendEmailSerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK


class SendRoomMessageAPIView(generics.CustomCreateAPIView):
    """
    Send room message api view
    """

    serializer_create_class = serializers.SendRoomMessageSerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK
