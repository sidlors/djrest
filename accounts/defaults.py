from time import time

from django.conf import settings
from django.utils import timezone
from django.utils.crypto import get_random_string


def renewal_time():
    """
    Generate user renewal time
    """
    return int(time())


def verification_code():
    """
    Generate email verification code
    """
    return get_random_string(
        length=4, allowed_chars='0123456789'
    )


def expiration_time():
    """
    Generate code expiration time
    """
    return timezone.now() + timezone.timedelta(
        seconds=settings.SIGNUP_CODE_EXPIRE
    )
