from django.contrib import admin

from .models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    """
    User admin
    """

    exclude = ('renewed',)
    list_display = (
        'username', 'email',
        'isActive', 'created', 'modified',
    )
    list_filter = ('isActive',)

    readonly_fields = ('lastLogin',)
    search_fields = (
        'firstName', 'middleName',
        'lastName', 'username', 'email',
    )
