from rest_framework import status
from rest_framework.parsers import MultiPartParser
from rest_framework.views import APIView

from common import generics, mixins
from common.response import Response
from common.utils import get_object_or_404

from . import filters, serializers
from .authentication import PasswordResetTokenAuthentication
from .models import User
from .permissions import AllowAny


class SignUpUsernameAvailabilityAPIView(generics.CustomCreateAPIView):
    """
    Sign up username availability api view
    """

    permission_classes = (AllowAny,)
    serializer_create_class = serializers.SignUpUsernameAvailabilitySerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK


class SignUpSendCodeAPIView(generics.CustomCreateAPIView):
    """
    Sign up send code api view
    """

    permission_classes = (AllowAny,)
    serializer_create_class = serializers.SignUpSendCodeSerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK


class SignUpAPIView(generics.CustomCreateAPIView):
    """
    Sign up api view
    """

    permission_classes = (AllowAny,)
    serializer_create_class = serializers.SignUpSerializer

    exclude_response_data = True


class SignInAPIView(generics.CustomCreateAPIView):
    """
    Sign in api view
    """

    permission_classes = (AllowAny,)
    serializer_create_class = serializers.SignInSerializer

    exclude_response_data = False
    status_code = status.HTTP_200_OK

    def get_response_data(self, serializer):
        return {'token': serializer.instance.token}


class SocialSignInAPIView(SignInAPIView):
    """
    Social sign in api view
    """

    serializer_create_class = serializers.SocialSignInSerializer


class ProfileAPIView(mixins.RequestUserMixin,
                     generics.RetrieveUpdateAPIView):
    """
    Profile api view
    """

    serializer_list_class = serializers.ProfileSerializer
    serializer_update_class = serializers.ProfileUpdateSerializer


class AvatarChangeAPIView(mixins.RequestUserMixin,
                          generics.UpdateAPIView):
    """
    Avatar change api view
    """

    parser_classes = (MultiPartParser,)
    serializer_update_class = serializers.AvatarChangeSerializer

    def get_request_data(self):
        file = self.request.FILES.get('avatar')
        return {'avatar': file} if file else {}


class PasswordChangeAPIView(mixins.RequestUserMixin,
                            generics.CustomCreateAPIView):
    """
    Password change api view
    """

    serializer_create_class = serializers.PasswordChangeSerializer

    as_update = True
    exclude_response_data = True
    status_code = status.HTTP_200_OK


class PasswordResetSendEmailAPIView(generics.CustomCreateAPIView):
    """
    Password reset send email api view
    """

    permission_classes = (AllowAny,)
    serializer_create_class = serializers.PasswordResetSendEmailSerializer

    exclude_response_data = True
    status_code = status.HTTP_200_OK


class PasswordResetTokenValidationAPIView(APIView):
    """
    Password reset token validation api view
    """

    authentication_classes = (PasswordResetTokenAuthentication,)

    def post(self, request, *args, **kwargs):
        return Response()


class PasswordResetAPIView(mixins.RequestUserMixin,
                           generics.CustomCreateAPIView):
    """
    Password reset api view
    """

    authentication_classes = (PasswordResetTokenAuthentication,)
    serializer_create_class = serializers.PasswordResetSerializer

    as_update = True
    exclude_response_data = True
    status_code = status.HTTP_200_OK


class UserAPIView(generics.ListAPIView):
    """
    User api view
    """

    queryset = User.objects.all()
    serializer_list_class = serializers.UserListSerializer

    filter_class = filters.UserFilter

    ordering_fields = '__all__'
    search_fields = (
        'firstName', 'middleName',
        'lastName', 'username', 'email', 'phone'
    )


class UserDetailAPIView(generics.RetrieveDestroyAPIView):
    """
    User detail api view
    """

    queryset = User.objects.all()
    serializer_list_class = serializers.UserListSerializer

    lookup_field = 'pk'
    lookup_url_kwarg = 'user_id'
