from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework import status

from common.utils import get_object_or_none, unique_id
from common import errors

from ..models import User

from .facebook import FacebookClient


class FacebookSignIn:
    """
    Facebook sign in
    """

    def __init__(self, token):
        self.client = FacebookClient(access_token=token)

    def __get_or_create_user(self, data):
        fb_birthday = data['birthday'].split('/')

        username = 'fb{}'.format(data['id'])
        password = 'fb{}'.format(unique_id())

        birthday = '{}-{}-{}'.format(
            fb_birthday[2], fb_birthday[0], fb_birthday[1]  # yyyy-mm-dd
        )

        user = get_object_or_none(User, username=username)

        if not user:
            user = User(
                firstName=data['first_name'],
                middleName=data['middle_name'],
                lastName=data['last_name'],

                username=username,

                email=data['email'],
                birthday=birthday
            )

            user.set_password(password)
            user.save()

        return user

    def signin(self):
        required_fields = (
            'id', 'name', 'first_name',
            'middle_name', 'last_name',
            'email', 'birthday'
        )

        response = self.client.me()

        if response.status == status.HTTP_500_INTERNAL_SERVER_ERROR:
            raise serializers.ValidationError({
                'connection': errors.FACEBOOK_API_CONNECTION_ERROR
            })

        if response.status != status.HTTP_200_OK:
            raise serializers.ValidationError({
                'token': response.text if hasattr(response, 'text') else ''
            })

        data = response.json()
        facebook_fields = data.keys()

        if set(required_fields) != set(facebook_fields):
            msg = _(
                'Invalid %(social)s fields '
                '"%(facebook_fields)s", must be "%(required_fields)s".'
            )
            raise serializers.ValidationError({
                'token': msg % {
                    'social': 'facebook',
                    'facebook_fields': ', '.join(facebook_fields),
                    'required_fields': ', '.join(required_fields)
                }
            })

        return self.__get_or_create_user(data)
