import jwt
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import exceptions
from rest_framework.authentication import (
    BaseAuthentication, get_authorization_header)

from common import errors
from common.utils import get_object_or_none

from .models import User


class TokenAuthentication(BaseAuthentication):
    """
    Token authentication
    """

    keyword = 'Token'
    model = User

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        try:
            payload = jwt.decode(key, settings.SECRET_KEY)
        except Exception:
            raise exceptions.AuthenticationFailed(
                errors.INVALID_TOKEN
            )

        if 'rnd' not in payload.keys():
            raise exceptions.AuthenticationFailed(
                errors.INVALID_TOKEN
            )

        user = get_object_or_none(self.model, pk=payload.get('sub'))

        if not user:
            raise exceptions.AuthenticationFailed(
                errors.USER_DOES_NOT_EXIST
            )

        if not user.isActive:
            raise exceptions.AuthenticationFailed(
                errors.USER_INACTIVE
            )

        if payload.get('rnd') != user.renewed:
            raise exceptions.AuthenticationFailed(
                errors.USER_PASSWORD_CHANGED
            )

        return (user, key)

    def authenticate_header(self, request):
        return self.keyword


class PasswordResetTokenAuthentication(TokenAuthentication):
    """
    Password reset token authentication
    """

    def authenticate_credentials(self, key):
        try:
            payload = jwt.decode(key, settings.SECRET_KEY)
        except Exception:
            raise exceptions.AuthenticationFailed(
                errors.INVALID_TOKEN
            )

        if 'rst' not in payload.keys():
            raise exceptions.AuthenticationFailed(
                errors.INVALID_TOKEN
            )

        user = get_object_or_none(self.model, pk=payload.get('sub'))

        if not user:
            raise exceptions.AuthenticationFailed(
                errors.USER_DOES_NOT_EXIST
            )

        if not user.isActive:
            raise exceptions.AuthenticationFailed(
                errors.USER_INACTIVE
            )

        if payload.get('rst') != user.renewed:
            raise exceptions.AuthenticationFailed(
                errors.USER_PASSWORD_CHANGED
            )

        return (user, key)
