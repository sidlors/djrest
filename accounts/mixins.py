from django.utils import timezone

from .auth import auth_token


class SignInMixin(object):
    """
    Sign in mixin
    """

    def create(self, validated_data):
        instance = self.context['user']
        instance.lastLogin = timezone.now()
        instance.save()

        instance.token = auth_token(instance)
        return instance
