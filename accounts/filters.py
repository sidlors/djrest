from django_filters import rest_framework as filters

from .models import User


class UserFilter(filters.FilterSet):
    """
    User filter
    """

    usernameIcontains = filters.CharFilter(
        field_name='username', lookup_expr='icontains'
    )

    createdGt = filters.DateTimeFilter(
        field_name='created', lookup_expr='gt'
    )
    createdGte = filters.DateTimeFilter(
        field_name='created', lookup_expr='gte'
    )
    createdLt = filters.DateTimeFilter(
        field_name='created', lookup_expr='lt'
    )
    createdLte = filters.DateTimeFilter(
        field_name='created', lookup_expr='lte'
    )

    modifiedGt = filters.DateTimeFilter(
        field_name='modified', lookup_expr='gt'
    )
    modifiedGte = filters.DateTimeFilter(
        field_name='modified', lookup_expr='gte'
    )
    modifiedLt = filters.DateTimeFilter(
        field_name='modified', lookup_expr='lt'
    )
    modifiedLte = filters.DateTimeFilter(
        field_name='modified', lookup_expr='lte'
    )

    class Meta:
        model = User
        exclude = ('password', 'avatar', 'renewed', 'isActive')
