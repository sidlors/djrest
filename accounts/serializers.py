from django.conf import settings
from django.core.validators import RegexValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from common import errors, mixins, regex
from common.utils import get_object_or_none, to_object

from .auth import authenticate, passwd_token
from .clients import FacebookSignIn
from .mixins import SignInMixin
from .models import SignUpCode, User
from .tasks import send_passwd_reset_mail, send_signup_mail


class SignUpUsernameAvailabilitySerializer(serializers.Serializer):
    """
    Sign up username availability serializer
    """

    username = serializers.CharField(max_length=200)

    def validate_username(self, value):
        user = get_object_or_none(User, username=value)
        if user:
            raise serializers.ValidationError(
                errors.USERNAME_ALREADY_IN_USE
            )
        return value

    def create(self, validated_data):
        return to_object(validated_data)


class SignUpSendCodeSerializer(serializers.ModelSerializer):
    """
    Sign up send code serializer
    """

    def create(self, validated_data):
        instance = super().create(validated_data)

        send_signup_mail.delay(instance.code, instance.email)
        return instance

    class Meta:
        model = SignUpCode
        exclude = ('code', 'expired')


class SignUpSerializer(serializers.ModelSerializer):
    """
    Sign up serializer
    """

    password = serializers.CharField(
        max_length=200,
        validators=[
            RegexValidator(regex.PASSWORD)
        ]
    )

    code = serializers.CharField(max_length=4)

    def validate(self, attrs):
        obj = get_object_or_none(
            SignUpCode,
            email=attrs['email'],
            code=attrs['code']
        )
        if not obj:
            raise serializers.ValidationError({
                'code': errors.INVALID_SIGN_UP_CODE
            })

        if not obj.is_valid:
            obj.delete()  # Delete sign up code
            raise serializers.ValidationError({
                'code': errors.INVALID_SIGN_UP_CODE
            })

        obj.delete()  # Delete sign up code
        attrs.pop('code')
        return attrs

    def create(self, validated_data):
        instance = User(**validated_data)
        instance.set_password(validated_data['password'])
        instance.save()
        return instance

    class Meta:
        model = User
        exclude = ('avatar', 'isActive', 'renewed')


class SignInSerializer(SignInMixin, serializers.Serializer):
    """
    Sign in serializer
    """

    username = serializers.CharField(max_length=200)
    password = serializers.CharField(max_length=200)

    def validate_username(self, value):
        user = get_object_or_none(User, username=value)
        if not user:
            raise serializers.ValidationError(
                errors.USER_DOES_NOT_EXIST
            )

        if not user.isActive:
            raise serializers.ValidationError(
                errors.USER_INACTIVE
            )

        self.context['user'] = user
        return value

    def validate(self, attrs):
        user = authenticate(
            password=attrs['password'],
            instance=self.context['user']
        )
        if not user:
            raise serializers.ValidationError({
                'password': errors.INCORRECT_PASSWORD
            })
        return attrs


class SocialSignInSerializer(SignInMixin, serializers.Serializer):
    """
    Social sign in serializer
    """

    social = serializers.CharField(max_length=10)
    token = serializers.CharField(max_length=300)

    def validate_social(self, value):
        socials = ('facebook',)
        if value not in socials:
            raise serializers.ValidationError(
                _('This social network is not implemented.')
            )
        return value

    def validate(self, attrs):
        if attrs['social'] == 'facebook':
            facebook = FacebookSignIn(token=attrs['token'])
            user = facebook.signin()

        if not user.isActive:
            raise serializers.ValidationError({
                'username': errors.USER_INACTIVE
            })

        self.context['user'] = user
        return attrs


class ProfileSerializer(mixins.QueryFieldsMixin,
                        serializers.ModelSerializer):
    """
    Profile serializer
    """

    class Meta:
        model = User
        exclude = ('password', 'isActive', 'renewed')


class ProfileUpdateSerializer(serializers.ModelSerializer):
    """
    Profile update serializer
    """

    class Meta:
        model = User
        fields = (
            'firstName', 'middleName',
            'lastName', 'birthday'
        )


class AvatarChangeSerializer(serializers.ModelSerializer):
    """
    Avatar change serializer
    """

    class Meta:
        model = User
        fields = ('avatar',)
        extra_kwargs = {'avatar': {'required': True}}


class PasswordChangeSerializer(serializers.Serializer):
    """
    Password change serializer
    """

    current = serializers.CharField(max_length=200)
    password = serializers.CharField(
        max_length=200,
        validators=[
            RegexValidator(regex.PASSWORD)
        ]
    )

    def validate_current(self, value):
        if not authenticate(
                password=value,
                instance=self.instance):
            raise serializers.ValidationError(
                errors.INCORRECT_PASSWORD
            )
        return value

    def validate(self, attrs):
        if attrs['current'] == attrs['password']:
            raise serializers.ValidationError({
                'password': errors.SAME_PASSWORD
            })
        return attrs

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class PasswordResetSendEmailSerializer(serializers.Serializer):
    """
    Password reset send email serializer
    """

    username = serializers.CharField(max_length=200)

    def validate_username(self, value):
        user = get_object_or_none(User, username=value)
        if not user:
            raise serializers.ValidationError(
                errors.USER_DOES_NOT_EXIST
            )

        self.context['user'] = user
        return value

    def create(self, validated_data):
        instance = self.context['user']
        token = passwd_token(instance)

        send_passwd_reset_mail.delay(
            token, instance.username, instance.email)
        return instance


class PasswordResetSerializer(serializers.Serializer):
    """
    Password reset serializer
    """

    password = serializers.CharField(
        max_length=200,
        validators=[
            RegexValidator(regex.PASSWORD)
        ]
    )

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class UserListSerializer(mixins.QueryFieldsMixin,
                         serializers.ModelSerializer):
    """
    User list serializer
    """

    class Meta:
        model = User
        exclude = ('password', 'isActive', 'renewed')
