import json

from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from .auth import auth_token, passwd_token
from .models import SignUpCode, User


class UserTestCase(TestCase):
    """
    User test case
    """

    fixtures = ('users',)

    def setUp(self):
        self.user = User.objects.get(username='user')
        self.token = auth_token(self.user)
        self.client = Client()

    def tearDown(self):
        self.user.delete()

    def test_password_hash(self):
        self.assertTrue(
            self.user.password.startswith('pbkdf2_sha256')
        )
        self.assertEqual(len(self.user.password), 78)

    def test_signup_username_availability(self):
        path = reverse('accounts_signup_username_availability', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'username': 'user2'
            }),
            content_type='application/json'
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_signup_send_code(self):
        path = reverse('accounts_signup_send_code', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'email': 'user2@mail.com'
            }),
            content_type='application/json'
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_signup(self):
        path = reverse('accounts_signup', kwargs={
            'version': 'v1'
        })

        signup_code = SignUpCode.objects.create(
            email='user2@mail.com'
        )

        response = self.client.post(
            path,
            json.dumps({
                'username': 'user2',
                'password': 'p455w0rd',
                'email': 'user2@mail.com',
                'code': signup_code.code
            }),
            content_type='application/json'
        )

        self.assertEqual(
            response.status_code, status.HTTP_201_CREATED
        )

    def test_signin(self):
        path = reverse('accounts_signin', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'username': 'user',
                'password': 'p455w0rd'
            }),
            content_type='application/json'
        )

        data = response.json()

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )
        self.assertTrue('token' in data.keys())

    def test_password_change(self):
        path = reverse('accounts_password_change', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'current': 'p455w0rd',
                'password': '12345678'
            }),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % self.token
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_password_reset_send_mail(self):
        path = reverse('accounts_password_reset_send_email', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'username': 'user'
            }),
            content_type='application/json'
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_password_reset_token_validation(self):
        path = reverse('accounts_password_reset_token_validation', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % passwd_token(self.user)
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_password_reset(self):
        path = reverse('accounts_password_reset', kwargs={
            'version': 'v1'
        })

        response = self.client.post(
            path,
            json.dumps({
                'password': 'p455w0rd'
            }),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % passwd_token(self.user)
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_me_profile_partial_update(self):
        path = reverse('accounts_me_profile', kwargs={
            'version': 'v1'
        })

        response = self.client.patch(
            path,
            json.dumps({
                'birthday': '1994-08-14'
            }),
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % self.token
        )

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )

    def test_user_list(self):
        path = reverse('accounts_user_list', kwargs={
            'version': 'v1'
        })

        response = self.client.get(
            path,
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % self.token
        )

        data = response.json()

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )
        self.assertTrue('results' in data.keys())
        self.assertTrue(isinstance(data['results'], list))

    def test_user_detail(self):
        path = reverse('accounts_user_detail', kwargs={
            'version': 'v1',
            'user_id': self.user.id
        })

        response = self.client.get(
            path,
            content_type='application/json',
            HTTP_AUTHORIZATION='Token %s' % self.token
        )

        data = response.json()

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )
        self.assertTrue('id' in data.keys())
        self.assertTrue(data['id'], self.user.id)
