from django.urls import re_path as path

from . import views


urlpatterns = [
    path(
        r'^signup/username-availability$',
        views.SignUpUsernameAvailabilityAPIView.as_view(),
        name='accounts_signup_username_availability'
    ),
    path(
        r'^signup/send-code$',
        views.SignUpSendCodeAPIView.as_view(),
        name='accounts_signup_send_code'
    ),
    path(
        r'^signup$',
        views.SignUpAPIView.as_view(),
        name='accounts_signup'
    ),
    path(
        r'^signin$',
        views.SignInAPIView.as_view(),
        name='accounts_signin'
    ),

    path(
        r'^social-signin$',
        views.SocialSignInAPIView.as_view(),
        name='accounts_social_signin'
    ),

    path(
        r'^me/profile$',
        views.ProfileAPIView.as_view(),
        name='accounts_me_profile'
    ),
    path(
        r'^me/avatar-change$',
        views.AvatarChangeAPIView.as_view(),
        name='accounts_avatar_change'
    ),

    path(
        r'^me/password-change$',
        views.PasswordChangeAPIView.as_view(),
        name='accounts_password_change'
    ),

    path(
        r'^me/password-reset/send-email$',
        views.PasswordResetSendEmailAPIView.as_view(),
        name='accounts_password_reset_send_email'
    ),
    path(
        r'^me/password-reset/token-validation$',
        views.PasswordResetTokenValidationAPIView.as_view(),
        name='accounts_password_reset_token_validation'
    ),
    path(
        r'^me/password-reset$',
        views.PasswordResetAPIView.as_view(),
        name='accounts_password_reset'
    ),

    path(
        r'^users$',
        views.UserAPIView.as_view(),
        name='accounts_user_list'
    ),
    path(
        r'^users/(?P<user_id>[a-zA-Z0-9-_]+)$',
        views.UserDetailAPIView.as_view(),
        name='accounts_user_detail'
    ),
]
