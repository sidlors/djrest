"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles import views as staticfiles_views
from django.urls import include, re_path as path
from django.utils.translation import gettext_lazy as _
from django.views.static import serve as static_serve

from . import views


admin.site.site_title = _('Django site admin')
admin.site.site_header = _('Django administration')
admin.site.index_title = _('Site administration')
admin.site.site_url = '/admin/'


urlpatterns = [
    # Admin
    path(
        r'^admin/',
        admin.site.urls
    ),

    # API v1
    path(
        r'^api/(?P<version>(v1))/accounts/',
        include('accounts.urls')
    ),
    path(
        r'^api/(?P<version>(v1))/common/',
        include('common.urls')
    ),

    # Media files
    path(
        r'^media/(?P<path>.*)$',
        static_serve,
        {
            'document_root': settings.MEDIA_ROOT
        },
        name='media-serve'
    ),
]

# Static files
if settings.DEBUG:
    urlpatterns.append(path(
        r'^static/(?P<path>.*)$',
        staticfiles_views.serve,
        name='static-serve'
    ))
else:
    urlpatterns.append(path(
        r'^static/(?P<path>.*)$',
        static_serve,
        {
            'document_root': settings.STATIC_ROOT
        },
        name='static-serve'
    ))

# 404 error
urlpatterns.append(path(
    r'^',
    views.not_found_view,
    name='not-found'
))
