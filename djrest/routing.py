from channels.routing import ProtocolTypeRouter, URLRouter
from django.urls import re_path as path

from common.routing import urlpatterns as common_routing

from . import consumers


application = ProtocolTypeRouter({
    'websocket': URLRouter([
        # WS v1
        path(
            r'^ws/v1/',
            URLRouter(common_routing)
        ),
        path(
            r'^',
            consumers.NotFoundConsumer,
            name='not-found'
        ),
    ])
})
